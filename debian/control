Source: criterion
Section: devel
Priority: optional
Maintainer: SZALAY Attila <sasa@debian.org>
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/sasa/criterion
Vcs-Git: https://salsa.debian.org/sasa/criterion.git
HomePage: https://github.com/Snaipe/Criterion
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 12),
               meson,
               cmake,
               libboxfort-dev (>= 0.1.2-2),
               libnanomsg-dev,
               libnanopb-dev,
               nanopb,
               libgit2-dev,
               libffi-dev,
               protobuf-compiler,
               python3-protobuf,
               gettext

Package: libcriterion3
Section: libs
Architecture: amd64 arm64 i386
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: cross-platform C and C++ unit testing framework
 Most test frameworks for C require a lot of boilerplate code to set up
 tests and test suites -- you need to create a main, then register new test
 suites, then register the tests within these suits, and finally call the
 right functions.
 .
 This gives the user great control, at the unfortunate cost of simplicity.
 .
 Criterion follows the KISS principle, while keeping the control the user
 would have with other frameworks:
 .
 ￼* C99 and C++11 compatible.
 ￼* Tests are automatically registered when declared.
 ￼* Implements a xUnit framework structure.
 ￼* A default entry point is provided, no need to declare a main unless you
    want to do special handling.
 ￼* Test are isolated in their own process, crashes and signals can be
    reported and tested.
 ￼* Unified interface between C and C++: include the criterion header and it
    just works.
 ￼* Supports parameterized tests and theories.
 ￼* Progress and statistics can be followed in real time with report hooks.
 ￼* TAP output format can be enabled with an option.
 ￼* Runs on Linux, FreeBSD, Mac OS X, and Windows (Compiling with MinGW GCC
    and Visual Studio 2015+).
 .
 This package contains the runtime library of the package

Package: libcriterion-dev
Section: libdevel
Architecture: amd64 arm64 i386
Multi-Arch: same
Depends: ${misc:Depends}, pkg-config, libcriterion3 (= ${binary:Version}), libboxfort-dev, libffi-dev, libgit2-dev, libnanomsg-dev
Description: cross-platform C and C++ unit testing framework (development files)
 Most test frameworks for C require a lot of boilerplate code to set up
 tests and test suites -- you need to create a main, then register new test
 suites, then register the tests within these suits, and finally call the
 right functions.
 .
 This gives the user great control, at the unfortunate cost of simplicity.
 .
 Criterion follows the KISS principle, while keeping the control the user
 would have with other frameworks:
 .
 ￼* C99 and C++11 compatible.
 ￼* Tests are automatically registered when declared.
 ￼* Implements a xUnit framework structure.
 ￼* A default entry point is provided, no need to declare a main unless you
    want to do special handling.
 ￼* Test are isolated in their own process, crashes and signals can be
    reported and tested.
 ￼* Unified interface between C and C++: include the criterion header and it
    just works.
 ￼* Supports parameterized tests and theories.
 ￼* Progress and statistics can be followed in real time with report hooks.
 ￼* TAP output format can be enabled with an option.
 ￼* Runs on Linux, FreeBSD, Mac OS X, and Windows (Compiling with MinGW GCC
    and Visual Studio 2015+).
 .
 This package contains the headers needed to compile other packages against
 the criterion library
